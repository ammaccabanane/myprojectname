import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-home-page',
  templateUrl: 'home-page.html',
})
export class HomePage {

  constructor(private navCtrl: NavController, private navParams: NavParams) {
  }

  navigateToSecondPage(): void{
    this.navCtrl.push('SecondPage', {
      message: "Hello from the homepage!"
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad fired once upon page loads');
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter is fired just as the page is about to become active');
  }

  ionViewDidEnter(){
    console.log('ionViewDidEnter is fired each time the page is entered');
  }

  ionViewWillLeave(){
    console.log('ionViewWillLeave is fired each time the page about to leaved');
  }

  ionViewDidLeave(){
    console.log('ionViewDidLeave is fired each time the user has left the page');
  }
  
  ionViewWilUnload(){
    console.log('ionViewDidLeave is run when the page is about to be destroyed');
  }

}
